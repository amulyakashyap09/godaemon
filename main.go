package main

import (
    "fmt"
    "os"
    "io/ioutil"
	"log"
	"path/filepath"
)

func getWorkingDirectory () string {
	pwd, err := os.Getwd()
    if err != nil {
        log.Fatal(err)
        os.Exit(1)
    }
	return pwd 
}

func readDirectory () []string {
	files, err := ioutil.ReadDir(".")
	if err!=nil{
		log.Fatal(err)
        os.Exit(1)
	}
	s:= make([]string, len(files))
	for i := range files {
		s = append(s, files[i].Name())
	}
	return s
}

func walkTheDir (dir string) {
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		
		if err != nil {
			fmt.Printf("prevent panic by handling failure accessing a path %q: %v\n", dir, err)
			return err
		}

		fmt.Printf("visited file: %q\n", path, info)
		return nil
	})

	if err != nil {
		fmt.Printf("hahaha error walking the path %q: %v\n", dir, err)
	}
}

func main (){
	var currDir string = getWorkingDirectory()
	walkTheDir(currDir)
	// var filesInDir []string = readDirectory()
    fmt.Println(currDir)
}
